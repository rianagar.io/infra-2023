package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	sessionDuration = time.Minute * 15
	adminUsername   = "admin"
	adminPassword   = "123456"
)

var (
	httpRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests.",
		},
		[]string{"method", "path"},
	)
)

func init() {
	prometheus.MustRegister(httpRequestsTotal)
}

func createRoutes(app *fiber.App, redisClient *redis.Client) {
	app.Get("/", func(handler *fiber.Ctx) error {
		httpRequestsTotal.WithLabelValues("GET", "/").Inc()
		return handler.SendString("Hello, world!")
	})

	app.Post("/", func(handler *fiber.Ctx) error {
		username := handler.Query("username", "invalid")
		password := handler.Query("password", "invalid")

		if username != adminUsername {
			httpRequestsTotal.WithLabelValues("POST", "/").Inc()
			return handler.Status(fiber.StatusBadRequest).SendString("invalid user")
		}

		if password != adminPassword {
			httpRequestsTotal.WithLabelValues("POST", "/").Inc()
			return handler.Status(fiber.StatusBadRequest).SendString("invalid password")
		}

		session := uuid.New()

		status := redisClient.Set(context.Background(), session.String(), "admin", sessionDuration)
		if status.Err() != nil {
			log.Printf("[ERROR] - Error creating session: %s", status.Err())

			httpRequestsTotal.WithLabelValues("POST", "/").Inc()
			return handler.Status(fiber.StatusInternalServerError).
				SendString("error creating sessions")
		}

		httpRequestsTotal.WithLabelValues("POST", "/").Inc()
		message := fmt.Sprintf("the new session is: %s", session)

		return handler.Status(fiber.StatusCreated).SendString(message)
	})

	app.Get("/exist/:session", func(handler *fiber.Ctx) error {
		session := handler.Params("session", "invalid")

		_, err := redisClient.Get(context.Background(), session).Result()
		if err != nil {
			if errors.Is(err, redis.Nil) {
				httpRequestsTotal.WithLabelValues("GET", "/exist/:session").Inc()
				return handler.Status(fiber.StatusNotFound).SendString("session does not exist")
			}

			log.Printf("[ERROR] - Error getting session: %s", err)

			httpRequestsTotal.WithLabelValues("GET", "/exist/:session").Inc()
			return handler.Status(fiber.StatusInternalServerError).
				SendString("error getting sessions")
		}

		httpRequestsTotal.WithLabelValues("GET", "/exist/:session").Inc()
		return handler.SendString("the session exists")
	})
}

func main() {
	app := fiber.New()

	redisClient := redis.NewClient(&redis.Options{ //nolint:exhaustive
		Addr:     "redis:6379",
		Password: "redis",
		DB:       0,
	})

	app.Use(requestLogger)

	app.Use("/metrics", promhttp.Handler())

	createRoutes(app, redisClient)

	err := app.Listen(":3000")
	if err != nil {
		log.Panicf("[ERROR] - Error listening on port 3000: %v", err)
	}
}

func requestLogger(c *fiber.Ctx) error {
	start := time.Now()

	if err := c.Next(); err != nil {
		return err
	}

	log.Printf("[INFO] - %s %s %s %v\n",
		c.IP(), c.Method(), c.Path(), time.Since(start))

	return nil
}
